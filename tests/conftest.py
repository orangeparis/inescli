from pytest import fixture


@fixture
def http_port():
    """ return the default http port"""
    return 8080
