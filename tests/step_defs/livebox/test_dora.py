from functools import partial
from pytest import fixture
from pytest_bdd import (
    scenario as bdd_scenario,
    given,
    when,
    then,
)
from gateway.services import Gateway


scenario = partial(bdd_scenario, "livebox/dora.feature")


@scenario("Check Dora Event")
def test_dora():
    """ Add """


@fixture
def result():
    return {}


@fixture
def gateway():
    g = Gateway("http://localhost:8080")
    yield g
    return


@given("A livebox fti/6ccauzq")
def livebox():
    return


@when("I Reboot it")
def when_reboot():
    return True



@then("I should have a Dora within 180 seconds")
def check_dora(gateway,result):
    result['result'] = gateway.wait_dora("fti/6ccauzq",180)
    assert result['result'].status_code == 200
