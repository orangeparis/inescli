@ines
Feature: Dora
  Check Dhcp Dora when rebooting a livebox

  Scenario: Check Dora Event
  Given The livebox fti/6ccauzq
  When I Reboot it
  Then I should have a Dora within 180 seconds
