# -- FILE: features/environment.py
from behave import fixture, use_fixture
from gateway.services import Gateway

# CASE FIXTURE-GENERATOR-FUNCTION (like @contextlib.contextmanager):
@fixture(name="ines_api")
def ines_api(context, *args, **kwargs):
    context.ines_api = Gateway("http://localhost:8080")
    yield context.ines_api


def before_tag(context, tag):
    if tag == "ines":
        use_fixture( ines_api, context, timeout=10)
