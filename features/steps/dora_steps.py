# -- FILE: features/steps/dora_steps.py
from behave import given, when, then, step

# note use context.ines_api


@given('The livebox {fti}')
def step_impl(context,fti):
    #context.fti= "fti/6ccauzq"
    context.fti = fti

@when('I Reboot it')
def step_impl(context):  # -- NOTE: number is converted into integer
    pass

@then('I should have a Dora within {timeout:d} seconds')
def step_impl(context,timeout):
    assert context.failed is False
    context.result = context.ines_api.wait_dora(context.fti, timeout)
    assert context.result.status_code == 200

