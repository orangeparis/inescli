import requests
import json

default_gateway_url = "http://localhost:8080"
default_timeout = 10


class Gateway(object):
    """


    """
    def __init__(self,url=None):
        """

        :param url:
        """
        if url is None:
            url = default_gateway_url
        self.url = url
        self.session = requests.Session()
        self.session.headers.update({'Content-Type': 'application/json'})



    def wait_dora(self,fti,timeout=None):
        """

        :param fti:
        :param timeout:
        :return:
        """
        uri = "/api/lbservices/dora/wait"
        url = self.url + uri
        if timeout is None:
            timeout = default_timeout
        param = {"fti":fti,"timeout":timeout}
        resp = self.session.post(url,data=json.dumps(param),timeout=timeout+2)
        print(resp.status_code)
        return resp



if __name__=="__main__":

    g = Gateway()

    r = g.wait_dora("fti/6ccauzq",120)
    if r.status_code == 200:
        data = r.json()
        print(data)
    print(r.text)